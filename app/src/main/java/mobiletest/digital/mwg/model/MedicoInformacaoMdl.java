package mobiletest.digital.mwg.model;

import java.io.Serializable;

public class MedicoInformacaoMdl implements Serializable {

    private String nomeFireBaseDB;
    private String descriptionFirebaDB;

    public String getNomeFireBaseDB() {
        return nomeFireBaseDB;
    }

    public void setNomeFireBaseDB(String nomeFireBaseDB) {
        this.nomeFireBaseDB = nomeFireBaseDB;
    }

    public String getDescriptionFirebaDB() {
        return descriptionFirebaDB;
    }

    public void setDescriptionFirebaDB(String descriptionFirebaDB) {
        this.descriptionFirebaDB = descriptionFirebaDB;
    }

    public String getPictureFireBaseDB() {
        return pictureFireBaseDB;
    }

    public void setPictureFireBaseDB(String pictureFireBaseDB) {
        this.pictureFireBaseDB = pictureFireBaseDB;
    }

    private String pictureFireBaseDB;



}
