package mobiletest.digital.mwg;

import android.content.Intent;
import android.graphics.Color;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;


import butterknife.BindView;
import butterknife.ButterKnife;
import mobiletest.digital.mwg.controller.CronometroControll;
import mobiletest.digital.mwg.controller.KCaloriaControll;
import mobiletest.digital.mwg.util.IsOnline;
import mobiletest.digital.mwg.view.InformacaoActivity;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.toolbarApp) Toolbar toolbar;

    @BindView(R.id.cronometro) TextView cronometro;

    @BindView(R.id.section_kcal) TextView kCaloria;

    @BindView(R.id.button) Button botao;

    @BindView(R.id.button2) Button botaoReset;

    @BindView(R.id.fabInformacao) Button fabInformacao;

    private CronometroControll cronometroControll = new CronometroControll();
    private KCaloriaControll kCaloriaControll = new KCaloriaControll();
    private static Handler handler = new Handler();
    private static boolean foiIniciado;

    public static long acumTempo;
    public static String acumVlBotao;

    private static int validaInicio;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);


        if (validaInicio==1){
            recuperaCronometro();
        }

        botao.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                acionaCronometro();
                validaInicio=1;
            }
        });
        botaoReset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                acumTempo=0;
                foiIniciado=false;
                botao.setText("Play");
                setColorResetButton();

                cronometroControll.setTempoInicial(System.currentTimeMillis());
                kCaloriaControll.setTempoEntrada(System.currentTimeMillis());
                cronometro.setText(cronometroControll.playInicial()); //Zera relógio
                kCaloria.setText(kCaloriaControll.playKCalorias()); //Zera contador Caloria
                handler.removeCallbacks(runnable);

            }
        });

        fabInformacao.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                    startActivity(new Intent(getBaseContext(), InformacaoActivity.class));
            }
        });
    }
    /*INíCIO
    ****Verificação Inicial de Conectividade e Aviso ao Usuário */
    @Override
    protected void onStart() {
        super.onStart();
        IsOnline isOnline = new IsOnline(getBaseContext());
        isOnline.isOn();
    }

    @Override
    protected void onPause() {
        super.onPause();
        IsOnline isOnline = new IsOnline(getBaseContext());
        isOnline.isOn();
    }
    /*Fim
    ****Verificação Inicial de Conectividade e Aviso ao Usuário */


    public void recuperaCronometro() {

        //CONTADOR PAUSADO
        //APERTA BOTAO 1-PLAY + 2-PAUSE + 3-FAB  + RETORNA:
        if (acumVlBotao.equals("Play") && acumTempo > 0){

            //Contador Atualiza Runnable com tempo do SO e acumulador do 2-PAUSE
            cronometroControll.setTempoInicial(System.currentTimeMillis()-(acumTempo*1000));
            kCaloriaControll.setTempoEntrada(System.currentTimeMillis()-(acumTempo*1000));
            cronometro.setText(cronometroControll.playInicial());
            kCaloria.setText(kCaloriaControll.playKCalorias());
            botao.setText("Play");
        }
        /*CONTADOR EXECUTANDO
        **APERTA BOTAO 1-PLAY + 2-FAB  + 3-RETORNA:
        **OBS.: (instancia inicial/Runnable em "acumTempo" em "ZERO" /
        ** necessário utilizar cronometroControll.getSegundosMilesimais() > 0 )
        */
        else if (acumVlBotao.equals("Pause") && cronometroControll.getSegundosMilesimais() > 0){
            acumTempo= cronometroControll.getSegundosMilesimais();
            cronometro.setText(cronometroControll.playInicial());
            kCaloria.setText(kCaloriaControll.playKCalorias());
            botao.setText("Pause");
            runnable.run();
        }
        //CONTADOR PAUSADO
        //APERTA BOTAO 1-PLAY + 2-RESET  + 3-RETORNA:
        else if (acumVlBotao.equals("Pause") && acumTempo == 0) {

            botao.setText("Play");
            handler.removeCallbacks(runnable);
        }
        //CONTADOR PAUSADO
        //BOTAO 1-PLAY + 2-PAUSE + 3-RESET  + 4-RETORNA:
        else if (acumVlBotao.equals("Play") && acumTempo == 0){
            acumTempo= cronometroControll.getSegundosMilesimais();
            botao.setText("Play");
            handler.removeCallbacks(runnable);
        }
    }

    private void acionaCronometro() {
        if (!foiIniciado){
            foiIniciado = true;
            cronometroControll.setTempoInicial(System.currentTimeMillis()-(acumTempo*1000));
            kCaloriaControll.setTempoEntrada(System.currentTimeMillis()-(acumTempo*1000));

            acumTempo= cronometroControll.getSegundosMilesimais();
            setColorResetButton();

            botao.setText("Pause");
            acumVlBotao = "Pause";
            runnable.run();

        } else {
            foiIniciado = false;
            cronometro.setText(cronometroControll.playInicial());
            kCaloria.setText(kCaloriaControll.playKCalorias());
            acumTempo= cronometroControll.getSegundosMilesimais();//trava Contador
            botao.setText("Play");
            acumVlBotao = "Play";
            handler.removeCallbacks(runnable);
        }
    }

    private void setColorResetButton() {

        if (foiIniciado){
            botaoReset.setTextColor(Color.parseColor("#000000"));
            botaoReset.setBackgroundColor(getResources().getColor(R.color.button_default));
        } else if (!foiIniciado){
            botaoReset.setTextColor(Color.parseColor("#ffffff"));
            botaoReset.setBackgroundColor(getResources().getColor(R.color.button_pressed));
        }
    }

    private Runnable runnable = new Runnable() {
        @Override
        public void run() {
            if (foiIniciado)
                cronometro.setText(cronometroControll.playInicial());
                kCaloria.setText(kCaloriaControll.playKCalorias());
                handler.postDelayed(runnable, 1000);
        }
    };
}
