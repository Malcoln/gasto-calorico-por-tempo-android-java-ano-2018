package mobiletest.digital.mwg.controller;

import android.content.Context;

public class KCaloriaControll {

    private static float kcalorias;
    private static float kcal;
    private static long segundosMilesimais;
    private static final String constante = "26666666";
    private static long tempoEntrada;
    public static final long SEGUNDOS_EM_MILESIMOS = 1000L;
    Context context;

    public KCaloriaControll() {

    }
    public static long getSegundosMilesimais() {
        return segundosMilesimais;
    }

    public static void setTempoEntrada(long tempoEntrada) {
        KCaloriaControll.tempoEntrada = tempoEntrada;
    }

    public String playKCalorias(){

        segundosMilesimais = (System.currentTimeMillis() - tempoEntrada)/ SEGUNDOS_EM_MILESIMOS;
        //1,6kcal=60s -> String constante = "26666666";
        kcalorias = (Long.parseLong(constante)*segundosMilesimais);
        kcal = kcalorias/1000000000;
        return String.format("%3.2f",kcal);
        //return String.valueOf(kcal);
    }
}
