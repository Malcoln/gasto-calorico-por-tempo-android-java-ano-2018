package mobiletest.digital.mwg.controller;

import android.content.Context;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.squareup.picasso.Picasso;

import java.util.Map;

import mobiletest.digital.mwg.model.MedicoInformacaoMdl;

public class FirebaseController {

    private FirebaseDatabase firebaseDatabase;
    private FirebaseStorage firebaseStorage;

    private MedicoInformacaoMdl medicoInformacaoMdl;

    private Context context;

    public static int validaDownFoto;

    public FirebaseController(Context contex){
        context = contex;
        medicoInformacaoMdl = new MedicoInformacaoMdl();

    }

    public void getFirebaseDatabase(final TextView Nome, final TextView Description, final ImageView view){


        firebaseDatabase = FirebaseDatabase.getInstance();
        DatabaseReference myRef = firebaseDatabase.getReference("DoctorMedicine");
        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                Map<String,String> map = (Map)dataSnapshot.getValue();

                medicoInformacaoMdl.setDescriptionFirebaDB(map.get("Description"));
                medicoInformacaoMdl.setNomeFireBaseDB(map.get("Name"));
                medicoInformacaoMdl.setPictureFireBaseDB(map.get("Picture"));

                Nome.setText(medicoInformacaoMdl.getNomeFireBaseDB());
                Description.setText(medicoInformacaoMdl.getDescriptionFirebaDB());

                firebaseStorage = FirebaseStorage.getInstance();
                StorageReference storageReference = firebaseStorage
                        .getReferenceFromUrl("gs://mobiletest-8f5a0.appspot.com")
                        .child(medicoInformacaoMdl.getPictureFireBaseDB());


                storageReference.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                    @Override
                    public void onSuccess(Uri uri) {
                        Picasso.with(context).load(uri.toString()).into(view);

                        validaDownFoto=1;
                    }
                });

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }


}
