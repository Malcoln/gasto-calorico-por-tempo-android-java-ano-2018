package mobiletest.digital.mwg.controller;

public class CronometroControll {

    public static final long SEGUNDOS_EM_MILESIMOS = 1000L;
    public static final long HORA_EM_MINUTOS = 60;
    private static final long MINUTOS_EM_SEGUNDOS = 60;

    public static long getSegundosMilesimais() {
        return segundosMilesimais;
    }

    private static long segundosMilesimais;
    private static long segundos;
    private static long minutos;
    private static long horas;



    public static void setTempoInicial(long tempoInicial) {
        CronometroControll.tempoInicial = tempoInicial;
    }

    private static long tempoInicial;



    public CronometroControll() {

    }

    public String playInicial(){
        segundosMilesimais = (System.currentTimeMillis() - tempoInicial) / SEGUNDOS_EM_MILESIMOS;
        segundos = (segundosMilesimais % MINUTOS_EM_SEGUNDOS);
        minutos = (segundosMilesimais / MINUTOS_EM_SEGUNDOS);
        horas = (minutos / HORA_EM_MINUTOS);
        if (minutos > 59){
            minutos = minutos - (horas * HORA_EM_MINUTOS);
        }
        return String.format("%02d:%02d:%02d", horas, minutos, segundos);
    }
}
