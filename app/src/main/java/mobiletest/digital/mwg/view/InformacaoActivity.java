package mobiletest.digital.mwg.view;

import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.squareup.picasso.Picasso;

import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import mobiletest.digital.mwg.R;
import mobiletest.digital.mwg.controller.FirebaseController;
import mobiletest.digital.mwg.model.MedicoInformacaoMdl;


public class InformacaoActivity extends AppCompatActivity {

    @BindView(R.id.toolbarApp) Toolbar toolbar;

    @BindView(R.id.teste) TextView nome;

    @BindView(R.id.descricao) TextView descricao;

    @BindView(R.id.foto) ImageView fotoInform;

    @BindView(R.id.swipeRefresh) SwipeRefreshLayout mSwipe;

    private FirebaseController firebaseController;

    private int getValida;

    private static Handler handler = new Handler();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_btn_informacao);

        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        firebaseController = new FirebaseController(getBaseContext());
        firebaseController.getFirebaseDatabase(nome, descricao, fotoInform);
        exibirProgresso();
    }

    public void exibirProgresso(){
        mSwipe.post(new Runnable() {
            @Override
            public void run() {

                getValida=firebaseController.validaDownFoto;
                if (getValida!=1) {
                    mSwipe.setRefreshing(true);
                    handler.postDelayed(this, 1000);
                }else if (getValida==1){
                    handler.removeCallbacks(this);
                    mSwipe.setRefreshing(false);
                }
            }

        });
    }

}

/*
medicoInformacaoMdl = new MedicoInformacaoMdl();

        firebaseDatabase = FirebaseDatabase.getInstance();
        DatabaseReference myRef = firebaseDatabase.getReference("DoctorMedicine");
        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                Map<String,String> map = (Map)dataSnapshot.getValue();

                medicoInformacaoMdl.setDescriptionFirebaDB(map.get("Description"));
                medicoInformacaoMdl.setNomeFireBaseDB(map.get("Name"));
                medicoInformacaoMdl.setPictureFireBaseDB(map.get("Picture"));

                teste.setText(medicoInformacaoMdl.getNomeFireBaseDB());
                descricao.setText(medicoInformacaoMdl.getDescriptionFirebaDB());

                getFotoStorage();

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }

    private void getFotoStorage() {
        firebaseStorage = FirebaseStorage.getInstance();
        StorageReference storageReference = firebaseStorage
                .getReferenceFromUrl("gs://mobiletest-8f5a0.appspot.com")
                .child(medicoInformacaoMdl.getPictureFireBaseDB());
        Toast.makeText(getBaseContext(),String.valueOf(storageReference),Toast.LENGTH_SHORT).show();

        storageReference.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
            @Override
            public void onSuccess(Uri uri) {
                Picasso.with(getBaseContext()).load(uri.toString()).fit().into(fotoInform);
            }
        });
    }
 */
