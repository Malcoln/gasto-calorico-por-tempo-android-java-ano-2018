package mobiletest.digital.mwg.view;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;

import mobiletest.digital.mwg.MainActivity;




/*
 *
 * Classe entrada Splash herda de classe "AppCompatActivity, responsável pela chamada da UI de entrada do app"
 *
 * metodo handler():
 * A Handler allows you to send and process {@link Message} and Runnable
 * objects associated with a thread's {@link MessageQueue}.  Each Handler
 * instance is associated with a single thread and that thread's message
 * queue.  When you create a new Handler, it is bound to the thread /
 * message queue of the thread that is creating it -- from that point on,
 * it will deliver messages and runnables to that message queue and execute
 * them as they come out of the message queue.
 */

public class SplashActivity extends AppCompatActivity {

    //Splash timer da tela
    private static int SPLASH_TIME_OUT = 1000;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                startActivity(new Intent(SplashActivity.this, MainActivity.class));
                // metodo chamando o contexto da Classe que irá controlar as views

                finish();
                //finaliza a thread;
            }
        }, SPLASH_TIME_OUT);

    }

}
